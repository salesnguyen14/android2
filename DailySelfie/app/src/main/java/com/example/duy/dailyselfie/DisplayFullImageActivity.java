package com.example.duy.dailyselfie;

import android.app.Activity;
import android.graphics.Bitmap;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ImageView;

import java.io.IOException;

/**
 * Created by duy on 5/21/15.
 */
public class DisplayFullImageActivity extends Activity {

    static final String TAG = "DISPLAY_FULL_IMAGE_ACTIVITY";
    private Bitmap mBitmap;

    @Override
    protected void onCreate (Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_view);
        Uri imageUri = getIntent().getData();

        if (imageUri != null) {

            ImageView imageView = (ImageView) findViewById(R.id.full_view);

            try {

                mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            }
            catch (IOException e) {

                Log.i(TAG, "Exception!");
                e.printStackTrace();
            }

            imageView.setImageBitmap(mBitmap);
        }

    }

}
