package com.example.duy.dailyselfie;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by duy on 5/20/15.
 */
public class ImageViewAdapter extends BaseAdapter {

    private static final String TAG = "IMAGEVIEW_ADAPTER";
    private ArrayList<SelfieRecord> mImageList = new ArrayList<SelfieRecord>();
    private static Context mContext;
    private static LayoutInflater inflater = null;

    public ImageViewAdapter (Context context) {
        mContext = context;
        inflater = LayoutInflater.from(mContext);
    }


    public int getCount() {
        return mImageList.size();
    }

    public Object getItem(int position) {
        return mImageList.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView (int position, View convertView, ViewGroup parent) {

        Log.i(TAG, "runnig getView");
        View newView = convertView;
        ViewHolder holder;
        SelfieRecord itemRecord = mImageList.get(position);

        if (null == convertView) {

            holder = new ViewHolder();
            newView = inflater.inflate(R.layout.selfie_badge_view, null);

            holder.selfie = (ImageView) newView.findViewById(R.id.selfie);
            holder.selfie_filename = (TextView) newView.findViewById(R.id.selfie_filename);
            newView.setTag(holder);

        } else {

            holder = (ViewHolder) newView.getTag();
        }

        holder.selfie.setImageBitmap(itemRecord.getmSelfieBitmap());
        holder.selfie_filename.setText(itemRecord.getFilename());

        return newView;
    }

    static class ViewHolder {

        ImageView selfie;
        TextView selfie_filename;

    }

    public void add(SelfieRecord newSelfie) {

        mImageList.add(newSelfie);
        this.notifyDataSetChanged();
    }

    public void removeAllViews() {

        mImageList.clear();
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

        for (File f : storageDir.listFiles()) {

            if (f.isFile()) {
                f.delete();
            }
        }

        notifyDataSetChanged();
    }


}
