package com.example.duy.dailyselfie;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends ListActivity {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final long TWO_MINUTES = 120000L; //ms
    static final String TAG = "MAIN_ACTIVITY";

    private String mCurrentPhotoPath;
    private ImageViewAdapter mAdapter;

    private File mStorageDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES);

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAdapter = new ImageViewAdapter(getApplicationContext());
        getListView().setAdapter(mAdapter);

        for(File fileInStorageDir : mStorageDir.listFiles()) {

            if (fileInStorageDir.isFile()) {

                mAdapter.add(new SelfieRecord("file:" + fileInStorageDir.getAbsolutePath(),
                        fileInStorageDir.getName()));
            }
        }

        getListView().setOnItemClickListener(
                new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {

                        Intent displaySelfie = new Intent();
                        SelfieRecord selfieRecord = (SelfieRecord) mAdapter.getItem(position);

                        displaySelfie.setClass(MainActivity.this, DisplayFullImageActivity.class);
                        displaySelfie.setData(Uri.parse(selfieRecord.getUri()));
                        startActivity(displaySelfie);

                    }
                });

        Log.i(TAG, "onCreate Finished");
        createNotification();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        Log.i(TAG, "Hello onCreateOptionsMenu");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.open_camera) {

            takePicture();
            return true;
        }
        else if (id == R.id.delete_all) {

            mAdapter.removeAllViews();
            Toast.makeText(this, "All pictures deleted", Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            Intent mediaScanIntent = new Intent (Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            File f = new File(mCurrentPhotoPath);

            this.sendBroadcast(mediaScanIntent);
            mAdapter.add(new SelfieRecord(mCurrentPhotoPath, f.getName()));
        }
    }

    private void takePicture() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File pictureFile = null;

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            try {

                pictureFile = createImageFile();

            } catch (IOException ex) {

                Toast.makeText(this, ex.toString(), Toast.LENGTH_SHORT).show();
            }

            if (pictureFile != null) {

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(pictureFile));
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }

        }
    }

    private File createImageFile() throws IOException {

        String imageFileName = new SimpleDateFormat("yyyy_MMdd_HHmm_ss").format(new Date());
        File image = File.createTempFile(imageFileName, ".jpg", mStorageDir);
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();

        return image;
    }

    private void createNotification() {

        Log.i(TAG, "Creating notification");

        Intent receiverIntent = new Intent(MainActivity.this, NotificationReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0,
                receiverIntent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + TWO_MINUTES, TWO_MINUTES, pendingIntent);

    }

}
