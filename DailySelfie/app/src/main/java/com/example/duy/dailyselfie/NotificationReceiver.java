package com.example.duy.dailyselfie;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

/**
 * Created by duy on 5/21/15.
 */
public class NotificationReceiver extends BroadcastReceiver {

    private static final String TAG = "NOTIFICATION_RCVR";

    @Override
    public void onReceive(Context context, Intent intent) {

        Log.i(TAG, "start onReceive");

        Intent NotificationIntent = new Intent(context, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                NotificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        Uri notificationSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification.Builder notificationBuild = new Notification.Builder(context)
                .setTicker("Time to take a pix!")
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(true)
                .setContentTitle("Selfie Time!")
                .setContentText("Take a picture or die")
                .setSound(notificationSound)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager)
                context.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1, notificationBuild.build());

    }
}
