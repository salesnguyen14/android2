package com.example.duy.dailyselfie;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by duy on 5/20/15.
 */
public class SelfieRecord {
    private String mSelfieFileName;
    private String mUri;
    private Bitmap mSelfieBitmap;


    public SelfieRecord(Bitmap selfieBitmap, String filename) {

        this.mSelfieFileName = filename;
        this.mSelfieBitmap = selfieBitmap;
    }

    public SelfieRecord(String uri, String filename) {

        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;

        try {

            URLConnection conn = new URL(uri).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        mSelfieBitmap = bm;
        mSelfieFileName = filename;
        mUri = uri;
    }

    public String getUri() {return mUri; }
    public String getFilename() { return mSelfieFileName; }
    public Bitmap getmSelfieBitmap() {return mSelfieBitmap; }

}
